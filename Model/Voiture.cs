namespace Model{
public class Voiture
    {

        public int Id { get; set; }

        public string Marque { get; set; }

        public string Modele {get; set; }
        
        public string Couleur { get; set; }

        public float Kilometrage { get; set; }

        
    }
}